<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    public function create(){
    	return view('posts.create');
    }
    public function store(Request $request){
    	//dd($request->all());
    	$request->validate([
 	   		'judul' => 'required|unique:pertanyaan2|max:255',
    		'isi' => 'required',
		]);
    	$query = DB::table('pertanyaan2')->insert([
    		"judul"=>$request["judul"],
    		"isi"=>$request["isi"]
    	]);
    		return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan');
    }
    public function index(){
    	$pertanyaan = DB::table('pertanyaan2')->get();
    	//dd($pertanyaan);
    	return view('posts.index', compact('pertanyaan'));
    }
    public function show($id){
    	$pertanyaan = DB::table('pertanyaan2')->where('id',$id)->first();
    	return view('posts.show', compact('pertanyaan'));
    }
    public function edit($id){
    	$pertanyaan = DB::table('pertanyaan2')->where('id',$id)->first();
    	return view('posts.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request){
    	$request->validate([
 	   		'judul' => 'required|unique:pertanyaan2|max:255',
    		'isi' => 'required',
		]);
    	$query = DB::table('pertanyaan2')->where('id',$id)
    	-> update([
    		'judul'=>$request['judul'],
    		'isi' =>$request['isi']
    	]);
    	return redirect('/pertanyaan')->with('success','Kamu berhasil update pos');
    }
    public function destroy($id){
    	$query =DB::table('pertanyaan2')->where('id',$id)->delete();
		return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus');
    }
}
