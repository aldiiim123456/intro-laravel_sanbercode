<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaan2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_pertanyaan2', function (Blueprint $table) {
            $table->unsignedBigInteger('poin');
            $table->unsignedBigInteger('profile_id');
            $table->unsignedBigInteger('pertanyaan2_id');
            $table->foreign('pertanyaan2_id')->references('id')->on('pertanyaan2');
            $table->foreign('profile_id')->references('id')->on('profile');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_pertanyaan2');
    }
}
