-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2020 at 01:29 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_sanbercode`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `tanggal_diperbaharui` date NOT NULL,
  `pertanyaan2_id` bigint(20) UNSIGNED NOT NULL,
  `profile_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `komentar_jawaban2`
--

CREATE TABLE `komentar_jawaban2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `jawaban_id` bigint(20) UNSIGNED NOT NULL,
  `profile_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `komentar_pertanyaan2`
--

CREATE TABLE `komentar_pertanyaan2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `pertanyaan2_id` bigint(20) UNSIGNED NOT NULL,
  `profile_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `like_dislike_jawaban2`
--

CREATE TABLE `like_dislike_jawaban2` (
  `poin` bigint(20) UNSIGNED NOT NULL,
  `jawaban_id` bigint(20) UNSIGNED NOT NULL,
  `profile_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `like_dislike_pertanyaan2`
--

CREATE TABLE `like_dislike_pertanyaan2` (
  `poin` bigint(20) UNSIGNED NOT NULL,
  `profile_id` bigint(20) UNSIGNED NOT NULL,
  `pertanyaan2_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(76, '2020_09_10_162711_create_komentar_pertanyaan_table', 1),
(77, '2020_09_10_162739_create_komentar_jawaban_table', 1),
(78, '2020_09_10_162802_create_like_dislike_jawaban_table', 1),
(126, '2014_10_12_000000_create_users_table', 2),
(127, '2014_10_12_100000_create_password_resets_table', 2),
(128, '2019_08_19_000000_create_failed_jobs_table', 2),
(129, '2020_09_10_162505_create_profile_table', 2),
(130, '2020_09_10_162638_create_jawaban_table', 2),
(131, '2020_09_11_001936_create_pertanyaan2_table', 2),
(132, '2020_09_11_020042_create_komentar_pertanyaan2_table', 2),
(133, '2020_09_11_020128_create_komentar_jawaban2_table', 2),
(134, '2020_09_11_020151_create_like_dislike_pertanyaan2_table', 2),
(135, '2020_09_11_020210_create_like_dislike_jawaban2_table', 2),
(136, '2020_09_11_024556_add_foreign_pertanyaan2_id_to_jawaban', 3),
(137, '2020_09_11_024807_add_foreign_profile_id_to_jawaban', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan2`
--

CREATE TABLE `pertanyaan2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date DEFAULT NULL,
  `tanggal_diperbaharui` date DEFAULT NULL,
  `jawaban_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pertanyaan2`
--

INSERT INTO `pertanyaan2` (`id`, `judul`, `isi`, `tanggal_dibuat`, `tanggal_diperbaharui`, `jawaban_id`, `profile_id`) VALUES
(9, 'APA AJA DAH', 'Jawaban Nomor Satu apa', NULL, NULL, NULL, NULL),
(10, 'nanyananya', 'ajajajaja', NULL, NULL, NULL, NULL),
(11, 'Nanya Lagi', 'Jawaban Nomor Dua apa?', NULL, NULL, NULL, NULL),
(12, 'nanya terossss', 'bacooodddd', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jawaban_pertanyaan2_id_foreign` (`pertanyaan2_id`),
  ADD KEY `jawaban_profile_id_foreign` (`profile_id`);

--
-- Indexes for table `komentar_jawaban2`
--
ALTER TABLE `komentar_jawaban2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `komentar_jawaban2_jawaban_id_foreign` (`jawaban_id`),
  ADD KEY `komentar_jawaban2_profile_id_foreign` (`profile_id`);

--
-- Indexes for table `komentar_pertanyaan2`
--
ALTER TABLE `komentar_pertanyaan2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `komentar_pertanyaan2_profile_id_foreign` (`profile_id`),
  ADD KEY `komentar_pertanyaan2_pertanyaan2_id_foreign` (`pertanyaan2_id`);

--
-- Indexes for table `like_dislike_jawaban2`
--
ALTER TABLE `like_dislike_jawaban2`
  ADD KEY `like_dislike_jawaban2_jawaban_id_foreign` (`jawaban_id`),
  ADD KEY `like_dislike_jawaban2_profile_id_foreign` (`profile_id`);

--
-- Indexes for table `like_dislike_pertanyaan2`
--
ALTER TABLE `like_dislike_pertanyaan2`
  ADD KEY `like_dislike_pertanyaan2_pertanyaan2_id_foreign` (`pertanyaan2_id`),
  ADD KEY `like_dislike_pertanyaan2_profile_id_foreign` (`profile_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pertanyaan2`
--
ALTER TABLE `pertanyaan2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pertanyaan2_jawaban_id_foreign` (`jawaban_id`),
  ADD KEY `pertanyaan2_profile_id_foreign` (`profile_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `komentar_jawaban2`
--
ALTER TABLE `komentar_jawaban2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `komentar_pertanyaan2`
--
ALTER TABLE `komentar_pertanyaan2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `pertanyaan2`
--
ALTER TABLE `pertanyaan2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD CONSTRAINT `jawaban_pertanyaan2_id_foreign` FOREIGN KEY (`pertanyaan2_id`) REFERENCES `pertanyaan2` (`id`),
  ADD CONSTRAINT `jawaban_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);

--
-- Constraints for table `komentar_jawaban2`
--
ALTER TABLE `komentar_jawaban2`
  ADD CONSTRAINT `komentar_jawaban2_jawaban_id_foreign` FOREIGN KEY (`jawaban_id`) REFERENCES `jawaban` (`id`),
  ADD CONSTRAINT `komentar_jawaban2_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);

--
-- Constraints for table `komentar_pertanyaan2`
--
ALTER TABLE `komentar_pertanyaan2`
  ADD CONSTRAINT `komentar_pertanyaan2_pertanyaan2_id_foreign` FOREIGN KEY (`pertanyaan2_id`) REFERENCES `pertanyaan2` (`id`),
  ADD CONSTRAINT `komentar_pertanyaan2_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);

--
-- Constraints for table `like_dislike_jawaban2`
--
ALTER TABLE `like_dislike_jawaban2`
  ADD CONSTRAINT `like_dislike_jawaban2_jawaban_id_foreign` FOREIGN KEY (`jawaban_id`) REFERENCES `jawaban` (`id`),
  ADD CONSTRAINT `like_dislike_jawaban2_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);

--
-- Constraints for table `like_dislike_pertanyaan2`
--
ALTER TABLE `like_dislike_pertanyaan2`
  ADD CONSTRAINT `like_dislike_pertanyaan2_pertanyaan2_id_foreign` FOREIGN KEY (`pertanyaan2_id`) REFERENCES `pertanyaan2` (`id`),
  ADD CONSTRAINT `like_dislike_pertanyaan2_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);

--
-- Constraints for table `pertanyaan2`
--
ALTER TABLE `pertanyaan2`
  ADD CONSTRAINT `pertanyaan2_jawaban_id_foreign` FOREIGN KEY (`jawaban_id`) REFERENCES `jawaban` (`id`),
  ADD CONSTRAINT `pertanyaan2_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
