@extends('master')

@section('content')
			<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tulis Pertanyaanmu Dalam Form Di Bawah Ini!!!</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="post">
              	@csrf
                <div class="card-body">
                  <!--<div class="form-group">
                    <label for="idprofile">ID Profile</label>
                    <input type="number" class="form-control" id="idprofile" name="idprofile" placeholder="Masukkan IDmu Disini!">
                  </div>
                  <div class="form-group">
                    <label for="idpertanyaan">ID Pertanyaan</label>
                    <input type="number" class="form-control" id="idpertanyaan" name="idpertanyaan" placeholder="Masukkan ID Questionmu Disini!">
                  </div>-->
                  <div class="form-group">
                    <label for="judul">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul','')}}" placeholder="Masukkan Judul Pertanyaanmu Disini!">
                    @error('judul')
    					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi','')}}" placeholder="Masukkan Isi Pertanyaanmu Disini!">
                    @error('isi')
    					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>

                 <!--   <label>Tanggal Dibuat</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="date" class="form-control" name="tgldibuat" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" im-insert="false">
                  </div>
                  /.input group -->
                 <!--<label>Tanggal Diperbaharui</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="date" class="form-control" name="tglbaru" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" im-insert="false">
                  </div>
                  <div class="form-group">
                    <label for="isi">ID Jawaban</label>
                    <input type="number" class="form-control" id="jawabanid" name="jawabanid" placeholder="Masukkan Isi Jawaban Disini!">
                  </div> --> 
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
@endsection