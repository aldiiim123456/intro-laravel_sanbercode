@extends('master')

@section('content')
<div class="card">
              <div class="card-header bg-secondary">
                <h3 class="card-title">Fixed Header Table</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 300px;">
              	@if(session('success'))
              		<div class="alert alert-success">
              			{{ session('success') }}
              		</div>
              	@endif
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Judul Pertanyaan</th>
                      <th>Isi Pertanyaan</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $pertanyaan2)
                    	<tr>
                    		<td>{{$key+1}}</td>
                    		<td>{{$pertanyaan2 -> judul}}</td>
                    		<td>{{$pertanyaan2 -> isi}}</td>
                    		<td style="display: flex;">
                    			<a href="/pertanyaan/{{$pertanyaan2->id}}" class="btn btn-info btn-sm" style="width: 55px;">Show</a>
                    			<a href="/pertanyaan/{{$pertanyaan2->id}}/edit" class="btn btn-dark btn-sm ml-1"style="width: 55px;">Edit</a>
                    			<form action="/pertanyaan/{{$pertanyaan2->id}}" method="post">
                    				@csrf
                    				@method('DELETE')
                    				<input type="submit" name="delete" value="Delete" class="btn btn-danger btn-sm ml-1" style="width: 55px;">
                    			</form>
                    		</td>
                    	</tr>
                    	@empty
                    	<tr>
                    		<td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                    	</tr>
                    @endforelse
                  </tbody>
                </table>

              </div>
               <a class="btn btn-primary mb-2 mt-4 ml-2" href="/pertanyaan/create" style="width: 150px">Tambah Data</a>
              <!-- /.card-body -->
            </div>
@endsection